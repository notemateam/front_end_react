import React,  { Component , useState }  from "react"
import Typed from 'react-typed';
import '../style/settings.css'
import storage  from 'local-storage-fallback'
import {defaultStudyModeSecondsDelay} from  './options'

function getDelayValueFromStorage(){
    if(storage.getItem('study_delay')!==null){
        return Number(storage.getItem('study_delay'));
    }else{
        return defaultStudyModeSecondsDelay;
    }
}


export default function Settings(){
    const [delay,setDelay]=useState(getDelayValueFromStorage());
    console.log("storage.getItem('foo')");
    console.log(storage.getItem('foo'));
    function handleDelayChange(e){
        setDelay(e.target.value);
        storage.setItem('study_delay', e.target.value);
    }

    return(
        <div className="settings-container" >
            <div className="container">
                <div className="columns is-multiline is-centered is-vcentered " >
                   
                   
                    <div className="column is-one-quarter"></div>
                    <div className="column is-half has-text-centered">
                        <h1 className="notema-heading-study ">
                            <Typed 
                                    strings={['S e t t i n g s']}
                                    typeSpeed={30}
                                    backSpeed={30}
                                    backDelay={500}
                                    startDelay={0}
                                    fadeOut={true}
                                />
                        </h1>
                    </div>
                    <div className="column is-one-quarter" ></div>



                    <div className="column is-one-quarter"></div>
                    <div className="column is-half has-text-centered">
                        Seconds delay for Study Mode: {delay}
                    </div>
                    <div className="column is-one-quarter"></div>


                    <div className="column is-one-quarter"></div>
                    <div className="column is-half has-text-centered">
                        <input className="slider is-fullwidth is-info" onChange={handleDelayChange} value={delay} step="1" min="5" max="60" type="range"/>
                    </div>
                    <div className="column is-one-quarter"></div>
                </div>
            </div>
        </div>
    )
}