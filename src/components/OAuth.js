import axios from 'axios'
import {OAuthGoogle} from './options'
import {isAuth,setToken} from './auth'
import Swal from 'sweetalert2'

export function googleAuth(response){

    if(response.tokenId){
       
    
        console.log(response);
    
        const data= {
            tokenId:response.tokenId,
            email:response.profileObj.email,
            familyName:response.profileObj.familyName,
            givenName:response.profileObj.givenName,
            googleId:response.profileObj.googleId,
            imageUrl:response.profileObj.imageUrl,
            name:response.profileObj.name,
            id_token:response.tokenId
        };
        // api/oauth/google...
        axios.post(OAuthGoogle,data).then((response)=>{
            console.log("OAuth OK!");
            console.log(response);
            setToken(response.data.token);
            if(isAuth()){
                window.location.reload();
            }
        })
    
              
    }
    
      

    
}