import  React,{  useState } from 'react'
import ReactDOM from 'react-dom'
// import _ from 'underscore'
import axios from 'axios';

import Cards from './cards'
import {isAuth, getToken} from './auth'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import storage  from 'local-storage-fallback'

var globalTimeout = null; 


export default function Notes(){
const [token,setToken]=useState(getToken());
const [note, setNote]=useState('');





    if(!isAuth()){
      return (
        <Redirect to='/login' />
      )
    }


    return (
        <div className="container">
            
            <Cards />
          
            
        </div>
        )   
}

function delay(callback, ms) {
    var timer = 0;
    return function() {
      var context = this, args = arguments;
      clearTimeout(timer);
      timer = setTimeout(function () {
        callback.apply(context, args);
      }, ms || 0);
    };
  }


  function bind(func, context) {
    return function() { // (*)
      return func.apply(context, arguments);
    };
  }