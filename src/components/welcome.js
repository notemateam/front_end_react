import React,  { Component } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import Typed from 'react-typed';


export default function Welcome(){


  var typed = new Typed('.element', {
    strings: ["First sentence.", "Second sentence."],
    typeSpeed: 30
  });
  
 

    return (
      <div className=" welcome-container">
        <div className="welcome-text-container">
            <div className="columns">
            
            <div className="column" ></div>
      
            <div className="column is-four-fifths  has-text-centered" >
              <h1 className="notema-heading ">
              <Typed 
                    strings={[
                      
                      'N o t e m a'
                  
                  
                  ]}
                    typeSpeed={100}
                    backSpeed={100}
                    backDelay={10000}
                    startDelay={1000}
                    fadeOut={true}
                    loop={true}
                />
                
              </h1>
            </div>
            <div className="column" ></div>
          </div>
          <div className="notema-line"></div>


          <div className="columns">
            <div className="column" ></div>
            <div className="column is-four-fifths color-white has-text-centered" >
                <div className="with-notema">
                  <Typed
                        strings={[
                          'Notes from any device',
                          'For Study',
                          // 'For Business',
                          'For You',
                          'With Notema you can create collections of your notes',
                          // 'With Notema you can export and import notes with xlsx files',
                          'With Notema you can share your notes collections with other people',
                          'With "Notema Study Mode"',                 
                          
                        ]}
                        backDelay={5000}
                        typeSpeed={30}
                        backSpeed={30}
                        fadeOut={true}
                        loop={true}
                    />
                </div>
            </div>
            <div className="column" ></div>
          </div>


        </div>
      </div>
    )
}