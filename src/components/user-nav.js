import React,  { Component,useState } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import storage  from 'local-storage-fallback' // if you use es6 or typescript


function logoutHandler(e){
    storage.clear();
    window.location.reload('/');
}


export default function UserNav(){
    return(
        <nav className="navbar" role="navigation" aria-label="main navigation">
        <Link className="navbar-item anone"  to="/">Notema</Link>
          <Link className="navbar-item anone" to="/notes">Notes</Link>
          <Link className="navbar-item anone" to="/study">Study</Link>
          <Link className="navbar-item anone" to="/settings">Settings</Link>
          <a className="navbar-item anone" onClick={logoutHandler} >Exit</a>
        </nav>
    )
}