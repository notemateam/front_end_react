import React, {
    useState,
    useEffect,
    componentDidMount,
} from 'react'
import ReactDOM from 'react-dom'
// import _ from 'underscore'
import {
    BrowserRouter as Router,
    Route,
    Link,
    Redirect,
    withRouter
} from "react-router-dom";
import {
    isAuth,
    getToken
} from './auth'
import {
    createnoteUrl,
    getNotesUrl,
    deleteNoteUrl
} from './options'
import storage from 'local-storage-fallback'
import Card from './card'
import axios from 'axios';
import Pagination from 'bulma-pagination-react';
import Swal from 'sweetalert2'
import withReactContent from 'sweetalert2-react-content'
import {LoadingAlert, ErrorAlert} from './alerts'


var globalTimeout = null; 

export default function Cards (){
    const [token, setToken] = useState(getToken());
    const [notes, setNotes] = useState([]);
    const [page,setPage]=useState(1);
    const [last_page,setLastPage]=useState(1);
    const [search_request,setSearchRequest]=useState('');

    const [get_notes_once,setNotesGetOnce]=useState(true);

    const [note, setNote]=useState('');

    const [total_notes,setTotalNotes]=useState(0);

    function handleNoteChange(e){
        setNote(e.target.value)
    }

    function handleSubmitNote(e){
        e.preventDefault();

        const Loading=LoadingAlert('Saving...')

        
        console.log('handleSubmitNote');
        axios.post(createnoteUrl,{
            note:note,
            token: token
        }).then((response)=>{
            Loading.close();

            setPage(1);
            setSearchRequest('');
            setTimeout(()=>{
                loadPositions(1,'');
            },50);
            console.log(response);
            resetCreateNoteForm();


            Swal.fire({
                position: 'center',
                type: 'success',
                title: 'Your note has been saved',
                showConfirmButton: false,
                timer: 2000
            })
        });





    }

    function resetCreateNoteForm(){
        setNote('');
    }


    function handleDeleteNote(e){
        var note_id=e.target.getAttribute('data-index');

        Swal.fire({
            title: 'Delete this note?',
            //text: "this note will be deleted",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete note'
          }).then((result) => {
            if (result.value) {

          

                axios.delete(deleteNoteUrl+'/?id='+note_id+'&token='+token)
                .then((data)=>{
                    console.log(data);
                    loadPositions(page,search_request);
                });

                Swal.fire({
                    type:'success',
                    timer: 2000,
                    text: 'Your Note has been deleted'
                })
            }
          })


        
    }

    function handlePageChange(page){
        console.log(page);
        if(page<=last_page && page>=1){
            setPage(page);
            loadPositions(page,search_request);
        }
    }

    function loadPositions(page,search_request){
        console.log('loadPositions');

        axios.get(getNotesUrl+'/?token='+token+'&page='+page+'&search_request='+search_request)
        .then((data)=>{
            console.log(data.data.data);
            setNotes(data.data.data);
            setLastPage(data.data.last_page);
            setTotalNotes(data.data.total);
        });
    }

    function handleSearch(e){
        console.log(e.target.value);
        var search_request=e.target.value;
        setSearchRequest(e.target.value)
        if (globalTimeout != null) {
            clearTimeout(globalTimeout)
        }
        globalTimeout = setTimeout(()=>{
        globalTimeout = null;
        loadPositions(page,search_request);
        setPage(1);
        }, 500);
    }
    
    console.log(window.navigator);

    useEffect(() => {
        if (get_notes_once) {
            console.log('ONCE');
            loadPositions(page,search_request);
            setNotesGetOnce(false);
        }
    });


    if (!isAuth()) {
        return (<Redirect to = '/login' />
        )
    }

    const rendernotes=notes.map((each,position)=>{
        return (
        <div key={'qwe'+each.id}  >
            <Card id={each.id} note={each.note} />
            <div className="has-text-centered">
                <a className="button is-link is-small " data-index={each.id} onClick={handleDeleteNote} >Delete</a>
            </div>
        </div>
        )
    })

    return ( 
        <div>
            <div>
                <h1 className="notema-heading-new-note">Create New Note</h1>
                <div className="notema-card">
                    <form onSubmit={handleSubmitNote}>
                      
                        <div className="styled-input wide">
                        <textarea className="create-new-note-textarea" value={note} onChange={handleNoteChange} maxLength={10000} ></textarea>
                        <label>Note</label>
                        <span></span>
                        </div>
                        <div className="has-text-centered">
                        <input type="submit" className="button is-link"  value="Create New Note" />
                        </div>    
                    </form>
                </div>
                <div className="notema-card">
                    <div className="has-text-centered search-notes">Search</div>
                    <input type="text" className="input is-large " value={search_request}  onChange={handleSearch}  placeholder="Search"/> 
                </div>

                <div className="notema-card">
                <div className="has-text-centered total-notes-info">Total: {total_notes}</div>
                    <Pagination
                    pages={last_page}
                    currentPage={page}
                    onChange={handlePageChange}
                    />
                </div>

            </div>
            {rendernotes}
                <div className="notema-card">
                <div className="has-text-centered total-notes-info">Total: {total_notes}</div>
                    <Pagination
                    pages={last_page}
                    currentPage={page}
                    onChange={handlePageChange}
                    />
                </div>
            
        </div>
    );

//   {rendernotes}
}