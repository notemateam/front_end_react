import  React,{  useState } from 'react'
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import axios from 'axios';
import {
  createnoteUrl,
  getNotesUrl,
  deleteNoteUrl,
  updateNoteUrl
} from './options'
import {
  isAuth,
  getToken
} from './auth'


console.log('navigator.cridentials');
console.log(navigator);

console.log(navigator.get);

var globalTimeout = null; 

export default function Card(props){
  const [token, setToken] = useState(getToken());
  const [title,setTitle]=useState(props.title);
  const [note,setNote]=useState(props.note);



  function handleUpdateNote(e){
    setNote(e.target.value)
    let note=e.target.value
    let id=1

    if (globalTimeout != null) {
        clearTimeout(globalTimeout)
    }
    globalTimeout = setTimeout(function() {
    globalTimeout = null;
    // delayed update after note change
    console.log('handleUpdateNote');
    console.log(note);
    axios.put(updateNoteUrl,{
      id:props.id,
      note:note,
      token:token
    })

    }, 500);
  }

    
    if (!isAuth()) {
        return (<Redirect to = '/login' />)
    }
  
  
    return(
        <div className="notema-card">
           
            <div className="styled-input wide">
              <textarea  onChange={handleUpdateNote} defaultValue={note} required></textarea>
              <label>Note</label>
              <span></span>
            </div>
        </div>
    )
}