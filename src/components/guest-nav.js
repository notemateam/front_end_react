import React,  { Component } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";


export default function GuestNav(){
    return(
        <nav className="navbar unselectable" role="navigation" aria-label="main navigation">
        <Link className="navbar-item anone"  to="/">Notema</Link>
          <Link className="navbar-item anone" to="/login">&nbsp;&nbsp;Login</Link>
          <Link className="navbar-item anone" to="/registration">&nbsp;&nbsp;Registration</Link>
        </nav>
    )
}