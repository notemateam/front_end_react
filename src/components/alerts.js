import Swal from 'sweetalert2'

export function LoadingAlert(msg='Loading...'){
   return  Swal.fire({
        title: msg,
        onBeforeOpen: () => {
          Swal.showLoading()
        }
      });
}
export function ErrorAlert(msg='Error...'){
  return  Swal.fire({
       title: msg,
       type: 'error'
     });
}

export function SuccessAlert(msg='Operation Succeeded',timer=3500){
  return Swal.fire({
    position: 'center',
    type: 'success',
    title: 'Your profile created!',
    showConfirmButton: false,
    timer: timer
  })
}