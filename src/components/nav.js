import React,  { Component } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import {isAuth} from './auth'
import UserNav from './user-nav'
import GuestNav from './guest-nav'


export default function Nav(){
    
    if(isAuth()){
        return <UserNav/>
    }else{
        return <GuestNav />
    }
    
}