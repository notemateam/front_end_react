import React,  { Component , useState, useEffect }  from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import axios from 'axios';
import { loginUrl, googleClientId,OAuthGoogle  } from './options'
import {isAuth,setToken} from './auth'
import { GoogleLogin } from 'react-google-login';
import {googleAuth} from './OAuth'
import {LoadingAlert,ErrorAlert} from './alerts'



var globalTimeout = null;
export default function Login(){
  const [email,setEmail]=useState('');
  const [password, setPassword]=useState('');

  function handleEmailChange(e){
    console.log('handleEmailChange');
    setEmail(e.target.value);
  }

  function handlePasswordChange(e){
    console.log('handlePasswordChange');
    setPassword(e.target.value)
  }

  useEffect(() => {
    const data= {
      'asdasdsd':123123,
    };
    axios.post(OAuthGoogle,data).then((response)=>{
      console.log(response);
    })
  })

  function LoginHandler(e){
    e.preventDefault();

    var Welcome=LoadingAlert('Loading');
    

    axios.post(loginUrl, {
      email: email,
      password: password,
      }).then((response)=>{
        Welcome.close();
        if(response.data.status=='ok'){
          setToken(response.data.token);
          if(isAuth()){
            window.location.reload();
          }
        }else{
          ErrorAlert('wrong email or password');
        }
        // console.log(response);
      }).catch(function (error) {
        console.log(error.response.status);
        // handle error
        ErrorAlert("Error code: "+ error.response.status);
      })
  }

  const responseGoogle = (response) => {
    googleAuth(response);
  }

  if(isAuth()){
    return (
      <Redirect to='/notes' />
    )
  }
  
  return (
    <div className="container">
    <form onSubmit={LoginHandler} >
        <div className="columns is-multiline is-centered is-vcentered ">
        
          <div className="column is-one-quarter"></div>
          <div className="column is-half  has-text-centered	">
            <h1 >Login</h1>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="styled-input login-input is-centered">
              <input type="text" onChange={handleEmailChange} required />
              <label>Email</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="styled-input login-input is-centered">
              <input type="password" onChange={handlePasswordChange} required />
              <label>Password</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>
          
          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="styled-input has-text-centered">
                <input type="submit" className="button is-link " value="Login" />
            </div>
          </div>
          <div className="column is-one-quarter"></div>
          

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="styled-input has-text-centered">
            <GoogleLogin 
                clientId={googleClientId}
                buttonText="Login with Google"
                onSuccess={responseGoogle}
                onFailure={responseGoogle}
                cookiePolicy={'single_host_origin'}
              />
            </div>
          </div>
          <div className="column is-one-quarter"></div>

        </div>
    </form>
 
    </div>

  )
}