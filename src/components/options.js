export const serverUrl='https://notema.appspot.com';
export const registrationUrl=serverUrl+'/api/registration';
export const loginUrl=serverUrl+'/api/login';
export const createnoteUrl=serverUrl+'/api/createnote'
export const getNotesUrl=serverUrl+'/api/get_notes'
export const getOneNote=serverUrl+'/api/get_one_note'
export const getIdsUrl=serverUrl+'/api/get_ids'

export const deleteNoteUrl=serverUrl+'/api/delete_note'

export const updateNoteUrl=serverUrl+'/api/update_note'

export const defaultStudyModeSecondsDelay=10

export const OAuthGoogle=serverUrl+'/api/oauth/google'
export const googleClientId="116370317417-t45otp8ibljshgofvt0ltr8cat3asm6b.apps.googleusercontent.com"