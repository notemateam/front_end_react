import React,  { Component , useState }  from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import {registrationUrl} from './options'
import {isAuth,setToken} from './auth'

import axios from 'axios';
import storage  from 'local-storage-fallback' // if you use es6 or typescript
import {SuccessAlert, LoadingAlert,ErrorAlert} from './alerts'


export default function Registration(){
  const [name, setName]=useState('');
  const [email,setEmail]=useState('');
  const [password, setPassword]=useState('');
  const [repeat_password, setRepeatPassword]=useState('');

  // storage.clear();


  function hanleNameChange(e){
    setName(e.target.value)
  }

  function hanleEmailChange(e){
    setEmail(e.target.value)
  }

  function hanlePasswordChange(e){
    setPassword(e.target.value)
  }

  function hanleRepeatPassword(e){
    setRepeatPassword(e.target.value)
  }

  function registrationFormValidation(){
    if(password!==repeat_password){
      alert('passwords do not match');
      return false;
    }else if(validateEmail(email)===false){
      alert('email is not valid');
      return false;
    }
    return true;
  }
  
  function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
  }
  
  function formRegistrationHandler(e){
    e.preventDefault();
    const Loading=LoadingAlert()

    if(registrationFormValidation()){
        axios.post(registrationUrl, {
        name: name,
        password: password,
        email: email
        })
        .then(function (response) {
          Loading.close();
          if(response.data['status']=='ok'){
            const Success=SuccessAlert();
            Success.then(()=>{
              resetRegistrationForm();
              setToken(response.data['token']);
              console.log('isAuth');
              console.log(isAuth());
              if(isAuth()){
                window.location.reload();
              }
            });

          }else{
            alert('email already in use')
          }
        })
        .catch(function (error) {
          console.log(error);
        });
    }
    //console.log(registrationUrl);
  }

  function resetRegistrationForm(){
    setName('');
    setEmail('');
    setPassword('');
    setRepeatPassword('');
  }

  


  if(isAuth()){
    return (
      <Redirect to='/notes' />
    )
  }

    return (
      <div className="container">
      <form onSubmit={formRegistrationHandler}>
        <div className="columns is-multiline is-centered is-vcentered ">
        <div className="column is-one-quarter"></div>
          <div className="column is-half has-text-centered	">
          <h1 >Registration</h1>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="registration-input styled-input is-centered">
              <input type="text" value={name}  onChange={hanleNameChange}  required />
              <label>Name</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="registration-input styled-input is-centered">
              <input type="text" value={email}  onChange={hanleEmailChange} required />
              <label>Email</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="registration-input styled-input is-centered">
              <input type="password" value={password}  onChange={hanlePasswordChange} required />
              <label>Password</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="registration-input styled-input is-centered">
              <input type="password" value={repeat_password}  onChange={hanleRepeatPassword}  required />
              <label>Repeat Password</label>
              <span></span>
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
          
          <div className="field has-text-left">
          
            <input type="checkbox" required />&nbsp;
            I agree to the <a href="#">terms and conditions</a><label className="checkbox ">
          </label>
         </div>
       
          </div>
          <div className="column is-one-quarter"></div>

          <div className="column is-one-quarter"></div>
          <div className="column is-half ">
            <div className="styled-input is-centered has-text-centered">
                <input type="submit" className="button is-link " value="Create Profile" />
            </div>
          </div>
          <div className="column is-one-quarter"></div>

          


        </div>
        </form>
        </div>
    )
  }