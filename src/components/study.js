import React, {useState,useEffect} from 'react'
import Typed from 'react-typed';
import axios from 'axios'
import {getIdsUrl,getOneNote} from './options'
import {useInterval,useTimeout} from './tools'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import {
  isAuth,
  getToken
} from './auth'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faPlayCircle,
  faPauseCircle,
  faWindowClose,
  faArrowAltCircleRight
  
} from '@fortawesome/free-solid-svg-icons'

import storage  from 'local-storage-fallback'
import {defaultStudyModeSecondsDelay} from  './options'
import {LoadingAlert,ErrorAlert} from './alerts'


function getDelayValueFromStorage(){
    if(storage.getItem('study_delay')!==null){
        return Number(storage.getItem('study_delay'));
    }else{
        return defaultStudyModeSecondsDelay;
    }
}

var next_not_pressed=true;

export default function Study(){
    const [token, setToken] = useState(getToken());
    const [studyvisibility,setStudyVisibility]=useState('invisible');



    const [note,setNote]=useState('');


    const [ids,setIds]=useState([]);
    const [history,setHistory]=useState([]);
    const [get_ids_once,setIdsOnce]=useState(true);
    const [currentnote,setCurrentNote]=useState(0);
    const [playCondition,setPlayCondition]=useState(true);


    function handleNextButton(){
      if(currentnote!=(ids.length-1)){
        getNoteData(ids[currentnote+1]);
        setCurrentNote(currentnote+1);
      }else{
        setCurrentNote(0);
        getNoteData(ids[0]);
      }
     
        
    }

    function handlePreviousButton(){

    }

    function handlePauseContinue(){
      if(playCondition){
        setPlayCondition(false);
      }else{
        setPlayCondition(true);
      }
    }

    function handleStudyModeVisibility(){
      if(note==''){
        getAllIds();
        getNoteData(ids[0]);
      }
      
      if(studyvisibility=='visible'){
        setStudyVisibility('invisible');
      }else{
        setStudyVisibility('visible');
        getNoteData(ids[currentnote]);
        setPlayCondition(true);
      }
    }

    function getAllIds(){
      axios.get(getIdsUrl+'/?token='+getToken()).then((data)=>{
        console.log("data.data");
        console.log(data.data);
        setIds(data.data);
      }); 
    }

    function getNoteData(id){
      axios.get(getOneNote+'/?id='+id+'&token='+token)
      .then((data)=>{
        console.log("getNoteData");

        console.log(data);
        setNote(data.data.note);
      });
    }
    console.log("ids");
    console.log(ids);

    //update all ids each 15 minutes
    useInterval(()=>{
      getAllIds();
    },900000);

    useInterval(()=>{
      if(playCondition){
        getNoteData(ids[currentnote]);
        if(currentnote!=(ids.length-1)){
          setCurrentNote(currentnote+1);
        }else{
          setCurrentNote(0);
        }
      }

    },getDelayValueFromStorage()*1000);

    useEffect(() => {
      if (get_ids_once) {
          getAllIds();
          getNoteData(ids[currentnote]);
          setIdsOnce(false);
      }
    });

    const notema_study_isvisible_class='notema-magic '+studyvisibility;

    function PlayPause(){
      if(playCondition){
        console.log("playCondition");
        console.log(playCondition);

        var icon=faPauseCircle 
      }else{
        console.log("playCondition");
        console.log(playCondition);
        var icon=faPlayCircle
      }
      return(
        <FontAwesomeIcon icon={icon} />
      )
    }



if (!isAuth()) {
  return (<Redirect to = '/login' />
  )
}

  
const StudyMode=()=>{
  return (
      <div className={notema_study_isvisible_class} >

      <div className="container">
              <div className="columns">
                <div className="column has-text-centered">
                <button  className="button  is-small is-white is-outlined study-mode-buttons is-pulled-left" onClick={handleStudyModeVisibility} ><FontAwesomeIcon icon={faWindowClose} /></button>              
                <button  className="button  is-small is-white is-outlined study-mode-buttons" onClick={handleNextButton} ><FontAwesomeIcon icon={faArrowAltCircleRight} />&nbsp;Next</button>              
   
                <button className="button is-small is-white is-outlined study-mode-buttons is-pulled-right" onClick={handlePauseContinue} ><PlayPause /></button>                                
                </div>
              </div>
              <div className="columns  has-text-centered">
              <div className="column is-full"><textarea className="color-white study-mode-note" style={{height: (window.innerHeight/100*60), minHeight: (window.innerHeight/100*60) }}  defaultValue={note} disabled ></textarea></div>
              </div>
          </div>
      </div>
  )
}



    return (
        <div className="study-container">
        <div className="container">
            <div className="columns">
            
            <div className="column" ></div>
      
            <div className="column is-four-fifths  has-text-centered" >
              <h1 className="notema-heading-study ">
              <Typed 
                    strings={['S t u d y&nbsp;&nbsp;&nbsp;&nbsp;M o d e']}
                    typeSpeed={30}
                    backSpeed={30}
                    backDelay={500}
                    startDelay={0}
                    fadeOut={true}
                />
              </h1>

            </div>
            <div className="column" ></div>
          </div>
            
          <div className="columns">
            <div className="column" ></div>
            <div className="column has-text-centered" >
                <a className="button is-white"  onClick={handleStudyModeVisibility}  to="#study" >Start</a>
            </div>
            <div className="column" ></div>      
          </div>
        </div>
        <StudyMode />
      </div>
    )
}
