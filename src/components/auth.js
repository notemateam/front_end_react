import storage  from 'local-storage-fallback'
export function isAuth(){
    return storage.getItem('token')!==null;
}

export function setToken(token){
    storage.setItem('token', token);
}

export function getToken(){
    return storage.getItem('token');
}