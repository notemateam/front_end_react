import React,  { Component } from "react"
import ReactDOM from 'react-dom'
import {
  BrowserRouter as Router,
  Route,
  Link,
  Redirect,
  withRouter
} from "react-router-dom";
import './style/default.css'
import Nav from './components/nav'

import Login from './components/login'
import Registration from './components/registration'
import Welcome from './components/welcome'
import Notes from './components/notes'
import Study from './components/study'
import Settings from './components/settings'




function App() {
  return (
    <div>
      <Router>
        <div>
          <Nav />
          <Route exact path="/" component={Welcome} />
          <Route path="/login" component={Login} />
          <Route path="/registration" component={Registration} />
          <Route path="/notes" component={Notes} />
          <Route path="/study" component={Study} />
          <Route path="/settings" component={Settings} />
        </div>
      </Router>

      <footer className="footer">
        <div className="content has-text-centered">
          <p>
          Notema
          </p>
        </div>
      </footer>
    </div>
  );
}

ReactDOM.render(<App/>,document.querySelector('#root'))